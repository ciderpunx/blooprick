# BloopRick

This is a userscript that gets rid of tweets from blue tick twitter accounts from your timeline.

## Installing

To install:

* Add [tampermonkey](https://www.tampermonkey.net/) to your browser.
* Click the [direct download link](https://gitlab.com/ciderpunx/blooprick/-/raw/main/Bloop%20Rick.user.js?inline=false) for the userscript.
* Click "install".

## Why BloopRick?

You'll figure it out. Hint: try saying it out loud.
