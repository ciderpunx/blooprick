// ==UserScript==
// @name         Bloop Rick
// @namespace    ox4
// @version      0.1
// @description  Remove tweets from "Twitter Blue" users from your timeline
// @author       Charlie Harvey
// @match        https://twitter.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    window.addEventListener('load', deBlueTick, false);
    window.addEventListener('scroll', deBlueTick, false);


    function ancestorGet(e,g) {
        return g==0
            ? e.parentElement
            : ancestorGet(e.parentElement,(g-1))
    }

    function deBlueTick() {
        document.querySelectorAll('[data-testid="icon-verified"] path:not([fill="#829aab"])').forEach(
            function(e) {
                console.log('Zapped another prick');
                ancestorGet(e,14).style.display="none";
            }
        );
    }
})();